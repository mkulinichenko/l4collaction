import java.util.*;

public class LessonCollaction {
    public static void main(String[] args) {
        String[] surname = {"Антонов","Шевченко","Бойко","Шевченко","Кравченко","Савицький","Антонов","Шевченко","Тетерів","Мамченко"};
        List<String> students = new ArrayList<>();
        for (String stud : surname){
            students.add(stud);
        }
        System.out.println(students.size());
        Set<String> uniqueStudents = new HashSet<>();
        for (String unStud : surname){
            uniqueStudents.add(unStud);
        }
        System.out.println(uniqueStudents.size());
        Map<String, Integer> numbersLetters = new TreeMap<>();
        for (String numLet : uniqueStudents){
            numbersLetters.put(numLet, numLet.length());
        }
        System.out.println(numbersLetters.get("Бойко"));
    }
}
